#include <stdio.h>

#define MY_CONST 0; /*Symbolic constants are declared at the top*/

int main()
{
    /*Product of Zero*/
    int a = 100000;
    int product = a * MY_CONST;
    printf("Value of is %6d but a*0 is %1d\n", a, product);
    return 0;
}