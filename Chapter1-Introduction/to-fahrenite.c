#include <stdio.h>

#define LOW 0
#define HIGH 600
#define STEP 20

int main()
{
    /*
        f and c are fahrenite and celcius values.
        c = (5/9)(f-32)
        f = 0, 20, 40, 60 .... 300
    */

    float c, f;

    f = LOW;
    printf("F\tC\n---------------\n");
    while(f <=HIGH)
    {
        c = (5.0/9.0)*(f-32);
        printf("%3.0f  %6.1f\n", f, c);
        f = f + STEP;
    }
}
