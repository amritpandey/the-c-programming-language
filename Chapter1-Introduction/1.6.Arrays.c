#include <stdio.h>

int main()
{

    int c, nwhite, nother, i;
    int ndigit[10];

    nwhite = nother = 0;
    for(i = 0; i < 10; ++i)
        ndigit[i] = 0;

    while((c=getchar()) != EOF){

        if (c >= '0' && c<= '9')
            ++ndigit[c-'0'];
        else if (c == ' ' || c == '\t' || c == '\n')
            ++nwhite;
        else
            ++nother;

    }
    printf("\n");
    for (i = 0; i < 10; ++i)
        printf("%d : %d\n", i, ndigit[i]);
    printf("White spaces: %d, Others: %d\n", nwhite, nother);

    return 0;
}