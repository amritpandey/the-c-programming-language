#include <stdio.h>

/*
Function declaration:
[return type] <function name>(parameters name[optional]);
This is also called as 👉`FUNCTION PROTOTYPE`
*/
int power(int m, int n);

/* Checking power function. */
int main()
{
    int i;
    for (i = 0; i <= 10; ++i)
        printf("%d %d %d\n", i, power(2, i), power(-3, i));

    return 0;
}

/* Defining power function */
int power(int base, int n)
{
    int i, p;
    p = 1;
    for (i = 1; i <= n; ++i)
        p = p * base;
    return p;
}