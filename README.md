# Learning to program in C

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

### Folder Structure

```
.
├── Chapters
├── Exercises
├── README.md
```
