/*
    Prepare a horizontal histogram for the length of words appearing in input.
    RECIPE:
        Combine Word Counting program(1.5.4) and Array program (1.6)
*/
#include <stdio.h>

#define OUT 0
#define IN 1

int main()
{
    /*
    nw: new word counter, state: IN or Out of word, low: length of a word,
    bw: big word
    */
    int c, nw, state, length_of_word, bw, i, j;

    /*lengths: An array to store length of words and occurance*/
    int lengths[10];
    for(i = 0; i < 10; ++i)
        lengths[i] = 0;

    
    state = OUT;
    nw = length_of_word = bw = 0;

    while((c = getchar()) != EOF){
        /*If space, tab or new line occur, we are out of word*/
        if (c == ' ' || c == '\t' || c == '\n') {
            state = OUT;
            /*If low is between 0 and 9 then append it in array.*/
            if (length_of_word > 0 && length_of_word < 10)
                ++lengths[length_of_word];
            /*Else just add one in big word.*/
            else
                ++bw;
            length_of_word = 0;
            ++nw;
        }
        /*Otherwise we are in and length of word is being counted*/
        else {
            state = IN;
            ++length_of_word;
        }
    }

    /*Histogram for words length between 1 and 9*/
    printf("\nTotal Words: %d\n", nw);
    printf("Length\tOccurance\n");
    for(i = 1; i < 10; ++i) {
        printf("%d\t", i);
        j = 1;
        while (j <= lengths[i]) {
            printf("*");
            ++j;
        }
        printf("\n");
    }
    /*Histogram for word length greater than 9*/
    printf(">9\t");
    for (i = 0; i <= bw; ++i)
        printf("*");
    printf("\n");

    return 0;
}