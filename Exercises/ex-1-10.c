#include <stdio.h>
#include <stdlib.h>

/*
Ex. 1. Make a program to copy input to its output.
    2. Replace tab, backspace, slash(/), with \t, \b, \\
*/

int main()
{
    int c;

    while((c = getchar()) != EOF)
    {
        /* Check if the input id tab, back or slash*/
        if(c == '\t')
            c = '\t';
        if(c == '\b')
            c = '\b';
        if(c == '\\')
            c = '\\';
        /*Copy input to output*/
        printf("%c", c);
    }

    return 0;
}