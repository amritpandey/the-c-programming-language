/* Exercise 1.15: Temperature conversion program with functions. */
#include <stdio.h>

#define LOWER   0
#define UPPER   300
#define STEP    20
/* function declarations */
int toCelcius(float f);
int toFahr(float c);

int main()
{
    int i;
    for(i = LOWER; i <= UPPER; i = i+STEP)
        printf("%d degree fahrenheit in celcius is %d.\n", i, toCelcius(i));

    return 0;
}
/* function definition */
int toCelcius(float fahr)
{
    float cel;
    cel = (5.0/9.0)*(fahr - 32);
    return cel;
}

int toFahr(float cel)
{
    float fahr;
    fahr = ((9.0/5.0)*cel) + 32;
    return fahr;
}