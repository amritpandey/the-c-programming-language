/*
	Exercise: Count blanks, tabs and new line.
*/
#include <stdio.h>

int main()
{
	int b, t, nl, c;
	b = 0; /* Initialise all the escape characters to 0 */
	nl = 0;
	t = 0;

	while((c = getchar()) != EOF){
		if(c == '\n')
			++nl;
		if(c == '\t')
			++t;
		if(c == ' ')
			++b;
	}

	printf("\nANALYSIS DONE.\nNew Lines: %d\nTabs: %d\nBlanks: %d\n", nl, t, b);
	return 0;
}