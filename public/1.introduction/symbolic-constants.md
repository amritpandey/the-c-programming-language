# Symbolic Constants

If there are constants in a program that are essential, and which convey some info about the program, these constants can be represented by pre declaring them at the top of the file with symbolic constants. In this way, we do not have to contain the constants in variables. Call these interpolation in C program.

```c
#define     SOME_CONSTANT       1000

main()
{
    int prod;
    prod = SOME_CONSTANT * 0;
}
```
